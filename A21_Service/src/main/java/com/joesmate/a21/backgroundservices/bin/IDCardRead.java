package com.joesmate.a21.backgroundservices.bin;

import android.app.Application;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

import com.joesmate.a21.backgroundservices.App;
import com.joesmate.sdk.util.ToolFun;

import vpos.apipackage.Fingerprint;
import vpos.apipackage.IDCard;
import vpos.apipackage.Sys;

/**
 * Created by andre on 2017/7/20 .
 */

public class IDCardRead {
    static final String TAG = IDCardRead.class.toString();
    //  static int m360fd = App.getInstance().m_360fd;

    App mApp = App.getInstance();
    /**
     * 显示界面
     */
    public void ShowActivity(Context context, Class<?> cls) {

        Intent intent = new Intent();
        intent.setClass(context,cls);
        intent.putExtra("action",8);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        context.startActivity(intent);
    }

    public  void CloseActivity(Application app)
    {
        Intent intent = new Intent("action.view");
        intent.putExtra("action", 1);
        app.sendBroadcast(intent);
    }

    private IDCardRead() {
    }

    private static final IDCardRead mInstance = new IDCardRead();

    public static IDCardRead getInstance() {

        return mInstance;
    }


    public byte[] ReadBaseMsg() { //读取身份证信息不带指纹
        //读取身份证信息  不带指纹
        byte[] idcard = new byte[1297];
        int i;

                i = IDCard.Lib_IDCardReadData(idcard, 0, 9);
                if (i == 0 ) {
                    IDCard.Lib_IDCardClose();
                    return idcard;
                } else {
                    IDCard.Lib_IDCardClose();
                    App.getInstance().tts.speak("读卡失败");
                    return idcard;
                }

    }
    public byte[] ReadBaseMsgFp() { //读取身份证信息带指纹
        Fingerprint.Lib_FpClose();//指纹下电
        //ToolFun.Dalpey(200);
        IDCard.Lib_IDCardOpen();//身份证上电
        Sys.Lib_Beep();//提示身份证准备好了可以刷卡
        byte[] idcard = new byte[2321];
        int i = IDCard.Lib_IDCardReadData(idcard, 1, 30);
        if (i == 0) {
            mApp.tts.speak("读卡成功");
            IDCard.Lib_IDCardClose();
            return idcard;
        } else {
            mApp.tts.speak("读卡失败");
            String str = String.valueOf(i);

            IDCard.Lib_IDCardClose();
            return ToolFun.hexStringToBytes(str);
        }
    }


}
