package com.joesmate.a21.backgroundservices;


import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.ActivityInfo;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.os.AsyncTask;
import android.os.Handler;
import android.os.Message;
import android.os.PowerManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;

import com.joesmate.a21.io.GPIO;

import com.joesmate.sdk.util.LogMg;
import com.joesmate.signaturepad.views.SignaturePad;
import com.jostmate.IListen.OnReturnListen;

import java.io.ByteArrayOutputStream;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;


public class SignaActivity extends AppCompatActivity {
    boolean isExite = true;
    boolean isTimeOut = false;
    final static String TAG = SignaActivity.class.toString();
    ExecutorService _ThreadPool = Executors.newSingleThreadExecutor();//消息队列
    SignaturePad signaturePad;
    OnReturnListen mlisten;

    Intent m_Intent;

    private int picHeight;
    private int picWidth;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        isTimeOut = false;
        isExite = false;
        timeOutTask.execute(60);//30秒

        m_Intent = getIntent();
        App.getInstance().EHandwriteGpio.onPower();
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_DISMISS_KEYGUARD,
                WindowManager.LayoutParams.FLAG_DISMISS_KEYGUARD);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON,
                WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
//        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);//强制横屏
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_signa);

        IntentFilter filter = new IntentFilter();
        filter.addAction("action.signature");
        registerReceiver(broadcastReceiver, filter);
          screenOn();
        mlisten = App.getInstance().getOnReturnListen();
        signaturePad = findViewById(R.id.signature_pad);
        signaturePad.setOnSignedListener(new SignaturePad.OnSignedListener() {
            @Override
            public void onStartSigning() {
                Log.i("开始", "开始签名");
            }

            @Override
            public void onSigned() {
                Log.i("结束", "签名结束");
            }

            @Override
            public void onClear() {
                Log.i("清除", "清除签名");
            }

            @Override
            public void onGetPaint(float v, float v1, float v2) {

            }
        });


    }


    @Override
    public void onWindowFocusChanged(boolean hasFocus) {
        super.onWindowFocusChanged(hasFocus);
        new Thread() {
            @Override
            public void run() {

                Message msg = handler.obtainMessage();
                msg.what = 3;//设置签名大小
                handler.sendMessage(msg);
            }
        }.start();
    }


    public void ClearClick(View v) {
        signaturePad.clear();
    }


    public void SaveClick(View v) {
        if (!signaturePad.isEmpty()) {

            new Thread() {
                @Override
                public void run() {


                    ByteArrayOutputStream baos = new ByteArrayOutputStream();
                    Bitmap bitmap = signaturePad.getSignatureBitmap();


                    Bitmap var2 = Bitmap.createBitmap(picWidth, picHeight, Bitmap.Config.ARGB_4444);
                    Canvas var3 = new Canvas(var2);
                    var3.drawColor(-1);
                    var3.drawBitmap(bitmap, 0.0F, 0.0F, (Paint) null);

                    var2.compress(Bitmap.CompressFormat.WEBP, 12, baos);
                    byte[] imgbuff = baos.toByteArray();
                    Intent intent = new Intent();
                    intent.putExtra("imgbuff", imgbuff);
                    // intent.putExtra("posbuff", str.getBytes());
                    intent.putExtra("bitmap", bitmap);
                    App.getInstance().tts.speak("获取成功");
                    mlisten.onSuess(intent);
                }
            }.start();
        } else {
            App.getInstance().tts.speak("获取失败");
            finish();
            mlisten.onErr(-1);
        }
        finish();
    }

    public void ExitClick(View v) {
        mlisten.onErr(-1);
        finish();
    }

    protected void onDestroy() {

        isExite = true;
        this.unregisterReceiver(broadcastReceiver);
        App.getInstance().EHandwriteGpio.offPower();
        super.onDestroy();
    }

    final BroadcastReceiver broadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            int tag = intent.getIntExtra("action", -1);
            {
                Message msg = handler.obtainMessage();
                msg.what = tag;
                msg.obj = intent;
                handler.sendMessage(msg);
                return;
            }
//            switch (tag) {
//                case 0://返回数据
//                {
//                    Message msg = handler.obtainMessage();
//                    msg.what = 0;
//                    msg.obj = intent;
//                    handler.sendMessage(msg);
//                }
//                break;
//                case 1://取消
//                {
//                    Message msg = handler.obtainMessage();
//                    msg.what = 1;
//                    msg.obj = intent;
//                    handler.sendMessage(msg);
//                }
//                break;
//                case 2: {
//                    Message msg = handler.obtainMessage();
//                    msg.what = 2;
//                    msg.obj = intent;
//                    handler.sendMessage(msg);
//                }
//                break;
//                case 4: {
//                    Message msg = handler.obtainMessage();
//                    msg.what = 4;
//                    msg.obj = intent;
//                    handler.sendMessage(msg);
//                }
//                break;
//            }
        }
    };
    final Handler handler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            switch (msg.what) {
                case 3: {

                    int h1 = signaturePad.getHeight();
                    int w1 = signaturePad.getWidth();
                    picHeight = h1;
                    picWidth = w1;
                    int h2 = m_Intent.getIntExtra("height", h1);
                    int w2 = m_Intent.getIntExtra("width", w1);
                    float scale = (float) h1 / (float) w1;
                    if (w1 > w2) {
                        picWidth = w2;
                        picHeight = (int) (picWidth * scale);
                    }
                    break;

                }
                case 4: {
                    SaveClick(null);
                    break;
                }
                case 0: {
                    Intent intent = (Intent) msg.obj;
                    if (!signaturePad.isEmpty()) {
                        ByteArrayOutputStream baos = new ByteArrayOutputStream();
                        Bitmap bitmap = signaturePad.getSignatureBitmap();
                        bitmap.compress(Bitmap.CompressFormat.WEBP, 12, baos);
                        byte[] jpg_buff = baos.toByteArray();
                        Intent _msg = new Intent();
                        _msg.putExtra("jpgbuff", jpg_buff);
                        mlisten.onSuess(intent);
                    } else {
                        mlisten.onErr(-1);
                    }
                    isExite=true;
                    finish();
                    break;
                }
                case 1: {
                    setResult(-1);
                    isExite=true;
                    finish();
                    break;
                }
                case 2: {
                    signaturePad.clear();
                    break;
                }


            }

        }
    };


    @Override
    protected void onResume() {
        /**
         * 设置为横屏
         */
        if (getRequestedOrientation() != ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE) {
//      setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_REVERSE_LANDSCAPE);
        }
        super.onResume();

    }

    private class paintRun implements Runnable {
        private Intent _intent;

        public paintRun(Intent intent) {
            _intent = intent;
        }

        @Override
        public void run() {

            mlisten.onRetPain(_intent);
        }
    }

    private final AsyncTask<Integer, String, String> timeOutTask = new AsyncTask<Integer, String, String>() {
        @Override
        protected String doInBackground(Integer... ints) {
            long timeout = ints[0] * 1000;
            long startTime = System.currentTimeMillis();

            while (true) {
                if (System.currentTimeMillis() - startTime > timeout) {
                    isTimeOut = true;

                    App.getInstance().tts.speak("签名超时，请重签");
                    finish();
                    break;
                }

                if (isExite) {
                    break;
                }
            }

            return "";
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            if (isTimeOut) {

                finish();
            }
        }
    };
    public void screenOn() {
        // turn on screen
        PowerManager mPowerManager = (PowerManager) getSystemService(POWER_SERVICE);
        PowerManager.WakeLock mWakeLock = mPowerManager.newWakeLock(PowerManager.SCREEN_BRIGHT_WAKE_LOCK | PowerManager.ACQUIRE_CAUSES_WAKEUP, "tag");
        mWakeLock.acquire();
        mWakeLock.release();
    }
}
