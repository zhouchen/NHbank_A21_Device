package com.joesmate.a21.backgroundservices.bin;


import android.util.Log;

import com.emv.CoreLogic;
import com.joesmate.a21.sdk.CMD;
import com.joesmate.a21.serial_port_api.libserialport_api;
import com.joesmate.sdk.util.LogMg;
import com.joesmate.sdk.util.ToolFun;

import vpos.apipackage.IDCard;
import vpos.apipackage.Icc;
import vpos.apipackage.Mcr;
import vpos.apipackage.Picc;
import vpos.apipackage.Sys;

/**
 * Created by andre on 2017/8/3 .
 */

public class PBOC {
    private static final PBOC mInstance = new PBOC();

    private void PBOC() {

    }


    public static PBOC getInstance() {
        IDCard.Lib_IDCardClose();
        return mInstance;
    }

    public String[] GetICCInfo( int ICType, String AIDList, String TagList, long Timeout) {

        ToolFun.Dalpey(200);
        String[] aryRet = null;
        int   iRet = FindCard(ICType,(int)Timeout*1000);
        if (iRet != -1) {
            ICType = iRet;
            if (AIDList == null || AIDList.equals(""))
                AIDList = "A000000333";
            aryRet = CoreLogic.GetICCInfo((byte) ICType, AIDList, TagList, Long.toString(Timeout));
            Log.e("A21接触卡",aryRet[1]);
        }
        return aryRet;
    }
//public String[] GetICCInfo( int ICType, String AIDList, String TagList, long Timeout) {
//
//Log.e("卡片类型",ICType+"");
//    String[] aryRet = null;
//    int   iRet=-2;
//  switch (ICType){
//      case 0:
//          Log.e("卡片类型111==",ICType+"");
//          iRet=FindICCard();
//          break;
//      case 1:
//          Log.e("卡片类型2222==",ICType+"");
//          iRet=FindNfcCard();
//          break;
//      case 2:
//          Log.e("卡片类型3333==",ICType+"");
//         iRet= FindCard(ICType,(int)Timeout*1000);
//          break;
//  }
//    if (iRet != -1) {
//        ICType = iRet;
//        if (AIDList == null || AIDList.equals(""))
//            AIDList = "A000000333";
//        aryRet = CoreLogic.GetICCInfo((byte) ICType, AIDList, TagList, Long.toString(Timeout));
//
//    }
//    return aryRet;
//}
    public String[] GetICCArqc(int ICType, String TxData, String aidlist, long strTimeout) {

        ToolFun.Dalpey(200);
        String[] aryRet = null;
        int      iRet = FindCard(ICType,(int)strTimeout*1000);
        if (iRet != -1) {
            ICType = iRet;
            aidlist = "A000000333";
            aryRet = CoreLogic.GetICCArqc((byte) ICType, TxData, aidlist, Long.toString(strTimeout));
        }
        Log.e("55yu=====",aryRet[0]+"00000"+aryRet[1]+"===="+aryRet[2]);
        Icc.Lib_IccClose((byte)0);
        Picc.Lib_PiccClose();
        return aryRet;
    }

    public String[] ARPCExeScript( int ICType, String Txdata, String ARPC, String CDol2) {

        ToolFun.Dalpey(200);
        String[] aryRet = null;
        int       iRet = FindCard(ICType,40000);
        if (iRet != -1) {
            ICType = iRet;
            LogMg.d("A21-PBOC", "ARPCExeScript--Txdata=%s ARPC=%s", Txdata, ARPC);
            aryRet = CoreLogic.ARPCExcScripts((byte) ICType, Txdata, ARPC, CDol2);
        }
        return aryRet;
    }

    public String[] GetTrDetail( int ICType, int NOLog, long strTimeout) {

        ToolFun.Dalpey(200);
        String[] aryRet = null;
        int     iRet = FindCard(ICType,(int)strTimeout*1000);
        if (iRet != -1) {
            ICType = iRet;
            aryRet = CoreLogic.GetICCTRXDetails((byte) ICType, NOLog, Long.toString(strTimeout));
        }
        return aryRet;
    }

    public String[] GetLoadLog( int ICType, int NOLog, String AIDList, long strTimeout) {

        ToolFun.Dalpey(200);
        String[] aryRet = null;
        int  iRet = FindCard(ICType,(int)strTimeout*1000);
        if (iRet != -1) {
            ICType = iRet;
            if (AIDList == null || AIDList.equals(""))
                AIDList = "A000000333";
            aryRet = CoreLogic.GetICCLoadDetails((byte) ICType, NOLog, AIDList, Long.toString(strTimeout));
        }
        return aryRet;
    }

    public String[] GetICAndARQCInfo( int ICType, String AIDList, String TagList, String TxData, long strTimeout) {

        ToolFun.Dalpey(600);
        long start = System.currentTimeMillis();
        String[] aryRet = new String[4];
        int      iRet = FindCard(ICType,(int)strTimeout*1000);
        if (iRet != -1) {
            ICType = iRet;
            if (AIDList == null || AIDList.equals(""))
                AIDList = "A000000333";
            String[] aryRet1 = CoreLogic.GetICCArqc((byte) ICType, TxData, AIDList, Long.toString(strTimeout));
            strTimeout = System.currentTimeMillis() - start;
            String[] aryRet2 = CoreLogic.GetICCInfo((byte) ICType, AIDList, TagList, Long.toString(strTimeout));
            if (aryRet1[0] == "0" && aryRet2[0] == "0") {
                aryRet[0] = aryRet1[0];
                aryRet[1] = aryRet1[1];
                aryRet[2] = aryRet2[1];
                aryRet[3] = aryRet2[2];
            }
        }
        return aryRet;
    }
        public String[] GetPSAM(int ICType, String AIDList, String TagList, long Timeout) {

       ToolFun.Dalpey(200);
        String[] aryRet = null;
        int   iRet = FindPSAMCard(ICType,(int)Timeout*1000);
        if (iRet != -1) {
            ICType = iRet;
            if (AIDList == null || AIDList.equals(""))
                AIDList = "A000000333";
            aryRet = CoreLogic.GetICCInfo((byte) ICType, AIDList, TagList, Long.toString(Timeout));
            if(aryRet==null){
                aryRet[0]="0";
                aryRet[1]="成功";
              }
        }
        return aryRet;
    }

    /**
     * 找卡
     *
     * @param IcType 找卡模式：0-IC卡接触卡座；1-IC卡非接触卡座；2-自动；
     * @return 找到卡的类型
     */
    private int FindCard(int IcType,int timeOut) {
        byte[] lpAtr = new byte[128];
        byte[] cardtype = new byte[1];
        byte[] uid = new byte[64];
        long  start = System.currentTimeMillis();
        while (System.currentTimeMillis() - start < timeOut) {
            switch (IcType) {
                case 0: {
                    if (FindICCard() == 0)

                        return 0;
                    break;
                }
                case 1: {

                    if (FindNfcCard() == 0)
                        return 1;
                    break;
                }
                case 2: {
                    Log.e("A21接触卡","222222222");
                    if (FindICCard() == 0)
                        return 0;
                    if (FindNfcCard() == 0)
                        return 1;
                    break;
                }

                default:  {
                    return -1;
                }
            }
        }
        return -1;
    }
     private int FindPSAMCard(int IcType,int timeOut) {
        byte[] lpAtr = new byte[128];
        byte[] cardtype = new byte[1];
        byte[] uid = new byte[64];
        long  start = System.currentTimeMillis();
        while (System.currentTimeMillis() - start < timeOut) {

          if(FindPSAMCar()==0) {
              return 0;
            }

        }
        return -1;
    }
    public int  FindICCard() {
        byte[] lpAtr = new byte[128];
        int  ret = Icc.Lib_IccCheck((byte)0);
        if (ret == 0) {
            ret = Icc.Lib_IccOpen((byte)0, (byte)1, lpAtr);

            if (ret == 0) {
                Log.e("FindICCard,成功", "FindICCard");
                return 0;
            }else{
                return ret;
            }
        }
        return -1;
    }

       public int  FindPSAMCar() {

        byte[] lpAtr = new byte[128];
        int  ret = Icc.Lib_IccCheck((byte)1);
        if (ret == 0) {
            ret = Icc.Lib_IccOpen((byte)1, (byte)1, lpAtr);

            if (ret == 0) {
                Log.e("FindPSAM,成功", "FindICCard");
                return 0;
            }else{
                return ret;
            }
        }
        return -1;
    }


    public int FindNfcCard() {
        Picc.Lib_PiccClose();
        byte[] cardtype = new byte[1];
        byte[] uid = new  byte[64];
        Picc.Lib_PiccOpen();

        int ret = Picc.Lib_PiccCheck((byte)'A', cardtype, uid);
        if (ret == 0) {
            Log.e("FindNfcCard,成功", "FindNfcCard");
            return 0;
        }
        return  ret;
    }


}
