package com.joesmate.a21.backgroundservices.bin;


import android.content.Intent;
import android.support.annotation.RequiresPermission;
import android.util.Log;

import com.joesmate.a21.backgroundservices.App;
import com.joesmate.a21.backgroundservices.DataProcessingService;
import com.joesmate.a21.backgroundservices.KeyboradActivity;
import com.joesmate.a21.backgroundservices.Main2Activity;
import com.joesmate.a21.backgroundservices.R;
import com.joesmate.a21.sdk.FingerDev;
import com.joesmate.a21.sdk.ReaderDev;
import com.joesmate.a21.sdk.WlFingerDev;
import com.joesmate.a21.serial_port_api.libserialport_api;
import com.joesmate.sdk.util.LogMg;
import com.joesmate.sdk.util.ToolFun;
import com.jostmate.IListen.OnReturnListen;

import java.io.UnsupportedEncodingException;
import java.sql.SQLSyntaxErrorException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.LinkedBlockingQueue;

import vpos.apipackage.Fingerprint;
import vpos.apipackage.IDCard;
import vpos.apipackage.Icc;
import vpos.apipackage.Key;
import vpos.apipackage.Mcr;
import vpos.apipackage.Pci;
import vpos.apipackage.Picc;
import vpos.apipackage.Sys;

/**
 * Created by andre on 2017/9/18 .
 */

public class DataProcessingRunnable implements Runnable {
    private final String TAG = getClass().getName();
    byte[] data;
    byte[] in;
    App mApp = App.getInstance();

    private DataProcessingRunnable() {
    }

    public DataProcessingRunnable(byte[] in_data, int fd) {
        in = in_data;
        data = getData(in_data);
    }

    @Override
    public void run() {

        int len = (in[1] & 0xff << 8) + (in[2] & 0xff);
        switch (data[0]) {
            //设备信息
            case (byte) 0xc0: {
                switch (data[1]) {
                    case (byte) 0x01: {//获取序列号
                        byte[] sn = new byte[32];
                        String snr;
                        int i = Sys.Lib_ReadSN(sn);
                        if (i == 0) {
                            SendReturnData(sn, sn.length);
                        }
                        break;
                    }
//修改序列号
                    case (byte) 0x02: {
                        int pos = 2;
                        int _len = data[pos] & 0xff;
                        byte[] h = new byte[_len];
                        System.arraycopy(data, ++pos, h, 0, _len);
                        String snr = new String(h);
                        ReaderDev.getInstance().setSnr(App.getInstance(), snr);
                        Sys.Lib_WriteSN("".getBytes());//写序列号  空值
                        int iRet = Sys.Lib_WriteSN(snr.getBytes());
                        if (iRet == 0)
                            sendOK();
                        else
                            sendErr();
                        break;
                    }
                    case (byte) 0x03: {
                        byte[] iRec = new byte[len - 2];
                        System.arraycopy(data, 2, iRec, 0, len - 2);
                        String strname = new String(iRec);
                        int iRet = DeviceData.setBtName(strname);
                        if (iRet == 0) {
                            sendOK();
                        } else {
                            sendErr();
                        }

                        break;
                    }
                    case (byte) 0x0d://设置时间
                    {
                        try {
                            DeviceData.getInstance().SetSysTime(data);
                            sendOK();
                        } catch (Exception ex) {
                            sendErr();
                            Log.e(TAG, ex.getMessage());
                        }
                        break;
                    }
                    case (byte) 0x0c://获取电量
                    {
//                        try {
//                            int p = DeviceData.getInstance().getPower();
//                            byte[] send = new byte[3];
//                            send[2] = (byte) p;
//                            SendReturnData(send, send.length);
//                        } catch (Exception ex) {
//                            sendErr();
//                            Log.e(TAG, ex.getMessage());
//                        }

                        break;
                    }
                    case (byte) 0x08: //蜂鸣
                        int i = Sys.Lib_Beep();

                        break;
                    case (byte) 0x09://获取标示符
                    {
                        try {
                            byte[] sn = new byte[32];
                            Sys.Lib_ReadSN(sn);

                            SendReturnData(sn, sn.length);
                        } catch (Exception ex) {
                            sendErr();
                            Log.e(TAG, ex.getMessage());
                        }
                        break;
                    }
                    case (byte) 0x0e: {//播放声音
                        try {
                            byte[] strdata = new byte[len - 2];
                            System.arraycopy(data, 2, strdata, 0, len - 2);
                            String s = new String(strdata);

                            mApp.tts.speak(s);
                            ToolFun.Dalpey(10);
                            sendOK();
                        } catch (Exception ex) {
                            sendErr();
                        }
                    }
                }
                break;
            }
            case (byte) 0x31: {
                switch (data[1]) {
                    case (byte) 0x11: {//固件版本号
                        try {
                            String ver = DeviceData.getVersionName();
                            byte[] version = new byte[4];

                            Sys.Lib_GetVersion(version);
                            SendReturnData(version, version.length);
                        } catch (Exception ex) {
                            sendErr();
                            Log.e(TAG, ex.getMessage());
                        }

                        break;
                    }

                }
                break;
            }
//身份证
            case (byte) 0x32: {
                switch (data[1]) {
                    case (byte) 0x50: {
                        try {

                            IDCardRead.getInstance().ShowActivity(App.getInstance().getApplicationContext(),Main2Activity.class);
                            App.getInstance().tts.speak("请放身份证");
                            Fingerprint.Lib_FpClose();//指纹下电
                            IDCard.Lib_IDCardOpen();//身份证上电
                            Sys.Lib_Beep();
                            byte[] send = IDCardRead.getInstance().ReadBaseMsg();
                            IDCardRead.getInstance().CloseActivity(App.getInstance());
                            if(send[0]!=0){
                                mApp.tts.speak("读卡成功");
                                IDCard.Lib_IDCardClose();
                                SendReturnData(send, send.length);
                                IDCardRead.getInstance().CloseActivity(App.getInstance());

                            }else{
                                IDCard.Lib_IDCardClose();
                                IDCardRead.getInstance().CloseActivity(App.getInstance());
                                sendErr();
                            }

                        } catch (Exception ex) {
                            IDCardRead.getInstance().CloseActivity(App.getInstance());
                            Log.e(TAG, ex.getMessage());
                        }
                        IDCardRead.getInstance().CloseActivity(App.getInstance());
                        break;
                    }
//磁条卡
                    case (byte) 0x60: {//读取卡信息
                        int code[] = new int[]{-1};
                        byte[] iData = MagneticCard.getInstance().ReadData(code);

                        if (code[0] > 0 && iData.length>250) {
                            SendReturnData(iData, iData.length);
                        } else {
                            mApp.tts.speak("读卡失败");
                            SendData(iData, iData.length);
                        }
                        break;
                    }
                    case (byte) 0x62: {//开始刷卡
                        MagneticCard.getInstance().ShowActivity(App.getInstance().getApplicationContext(),Main2Activity.class);
                        App.getInstance().tts.speak("请刷磁条卡");

                        Mcr.Lib_SelectMcr((byte) 0);
                        Mcr.Lib_McrOpen();
                        Mcr.Lib_McrReset();

                        int iRet = MagneticCard.getInstance().SlotCard();

                        MagneticCard.getInstance().CloseActivity(App.getInstance());
                        if (iRet < 0) {
                            sendErr();
                        }
                        else {
                            sendOK();
                        }
                        MagneticCard.getInstance().CloseActivity(App.getInstance());
                        break;
                    }
//ic卡和磁条卡轮询
                    case (byte)0x63:{//ic卡和磁条卡轮询
                    byte[] iData=   MagneticCard.getInstance().MagData();
                        if ( iData.length>250) {

//                            mApp.tts.speak("读卡成功");
                            SendReturnData(iData, iData.length);
                        } else {
//                            mApp.tts.speak("读卡失败11");
                            SendData(iData, iData.length);
                        }
                    }
                    break;
                }
                break;
            }
            case (byte) 0xD0: {
                switch (data[1]) {
                    case (byte) 0x06: {//读身份证
                        try {
                            App.getInstance().tts.speak(mApp.getString(R.string.PleaseDropIdCard));
                            byte[] send = IDCardRead.getInstance().ReadBaseMsg();
                            SendReturnData(send, send.length);
                        } catch (Exception ex) {
                            sendErr();
                            Log.e(TAG, ex.getMessage());
                        }
                        break;
                    }
                    case (byte) 0x07: {//读身份证+指纹
                        try {
                            App.getInstance().tts.speak(mApp.getString(R.string.PleaseDropIdCard));
                            byte[] send = IDCardRead.getInstance().ReadBaseMsgFp();
                            SendReturnData(send, send.length);
                        } catch (Exception ex) {
                            sendErr();
                            Log.e(TAG, ex.getMessage());
                        }
                        break;
                    }
                }
                break;
            }
            case (byte) 0xC6: {
                switch (data[1]) {
                    case (byte) 0x01://设置波特率
                    {
                        int iRet = PassThrough.getInstance().setBaud(data[2]);

                        if (iRet >= 0)
                            sendOK();
                        else
                            sendErr();
                        break;
                    }
                    case (byte) 0x02: {//获取指纹特征透传命令
                        switch (data[2]) {
                            case (byte) 0x00: {//透传
                                App.getInstance().tts.speak(mApp.getString(R.string.PleasePushFp));
                                //显示界面
                                PassThrough.getInstance().ShowActivity(App.getInstance().getApplicationContext(),Main2Activity.class);
                                        Sys.Lib_Beep();
                                IDCard.Lib_IDCardClose();
                                //ToolFun.Dalpey(200);
                                Fingerprint.Lib_FpOpen();

                                Fingerprint.Lib_SetFgBaudrate(9600);

                                ToolFun.Dalpey(200);

                                byte[] send = new byte[len - 3];
                                System.arraycopy(data, 3, send, 0, len - 3);
                                int iRet = PassThrough.getInstance().SendCMD(send);
                                //关闭界面
                                PassThrough.getInstance().CloseActivity(App.getInstance());
                                if (iRet >= 0){
                                    sendOK();
                                }
                                else{
                                    sendErr();
                                }
                                PassThrough.getInstance().CloseActivity(App.getInstance());
                                break;
                            }
                            case (byte) 0x01: {//得到数据返回信息
                                int[] recode = new int[]{-1};
                                byte[] returndata = PassThrough.getInstance().getBuffer(recode);
                                if (returndata[0]==126) {

                                    mApp.tts.speak("获取成功");

                                    SendReturnData(returndata, returndata.length);//成功

                                } else if(recode[0]==3011){
                                    SendData(returndata, returndata.length);//失败
                                }
                                break;
                            }
                            case (byte) 0x02: {
                                switch (data[3]) {
                                    case 0x00: {
                                        FingerDev fingerDev = new WlFingerDev();
                                      //  fingerDev.setDevFd(App.getInstance().m_fpfd);
                                        try {
                                            //fingerDev.ShowActivity(App.getInstance().getApplicationContext(), Main2Activity.class);
                                            byte[] returndata = fingerDev.imgFingerPrint();
                                            SendReturnData(returndata, returndata.length);
                                        } catch (Exception e) {
                                            sendErr();
                                        }

                                        break;
                                    }
                                    case 0x01:
                                        break;
                                    case 0x02:
                                        break;
                                    case 0x03:
                                        break;
                                    default:
                                        sendErr();
                                }
                                break;
                            }
                        }
                        break;
                    }
                    case (byte) 0x03: {//密码键盘
                        switch (data[2]) {//模式
                            case (byte) 0x00: {
                                //
                                int datalen = (data[5] & 0xff);
                                byte[] d = new byte[datalen];
                                System.arraycopy(data, 8, d, 0, datalen);
                                break;
                            }
                            case (byte) 0x01: {//获取密码

                                App.getInstance().tts.speak("请输入密码");
                                Sys.Lib_Beep();
                                try {
                                    Keyboard.getInstance().ShowActivity(App.getInstance().getApplicationContext(), KeyboradActivity.class, data, new OnReturnListen() {
                                        @Override
                                        public void onSuess(Intent intent) {
                                            Keyboard.getInstance().CloseActivity(App.getInstance());
                                         byte[]  send=   intent.getByteArrayExtra("pinBlock");
                                            SendReturnData(send, send.length);

                                        }
                                        @Override
                                        public void onRetPain(Intent intent) {
                                        }
                                        @Override
                                        public void onErr(int code) {

                                            Keyboard.getInstance().CloseActivity(App.getInstance());
                                            byte send[] = new byte[]{(byte) 0x01, (byte) 0x00};
                                            SendData(send, send.length);
                                        }
                                    });
                                }catch (Exception e){
                                    sendErr();
                                   }
                                Keyboard.getInstance().CloseActivity(App.getInstance());
                                break;
                            }
                            case (byte) 0x02: {
                                int iRet = Keyboard.getInstance().InitPinPad();
                                if (iRet == 0){
                                    Sys.Lib_Beep();
                                    sendOK();
                                }

                                else
                                    sendErr();
                                break;
                            }
                            case (byte) 0x03: {
                                int iRet = Keyboard.getInstance().DownMKey(data);
                                if (iRet == 0){

                                    mApp.tts.speak("主密钥下载成功");
                                    sendOK();
                                }

                                else {
                                    mApp.tts.speak("主密钥下载失败");
                                    sendErr();
                                }
                                break;

                            }
                            case (byte) 0x04: {
                               // Keyboard.getInstance().setFD(App.getInstance().m_360fd);
                                int iRet = Keyboard.getInstance().DownWKey(data);
                                if (iRet == 0) {
//                                    Sys.Lib_Beep();
                                    mApp.tts.speak("工作密钥下载成功");
                                    sendOK();
                                }
                                else {
                                    mApp.tts.speak("工作密钥下载失败");
                                    sendErr();
                                }
                                break;
                            }
                            case (byte) 0x05: {
                              //  Keyboard.getInstance().setFD(App.getInstance().m_360fd);
                                int iRet = Keyboard.getInstance().ActiveWKey(data);
                                if (iRet == 0)
                                    sendOK();
                                else
                                    sendErr();
                                break;
                            }
                            default:
                                sendErr();
                                break;

                        }
                        break;
                    }
                }
                break;

            }
            case (byte) 0xc7: {
                switch (data[1]) {
                    case (byte) 0x01: {//签名
                        try {

                            App.getInstance().tts.speak(mApp.getString(R.string.PleaseSign));
                            ToolFun.Dalpey(200);
                            Sys.Lib_Beep();
                            Signature.getInstance().Start(App.getInstance().getApplicationContext(), new OnReturnListen() {
                                @Override
                                public void onSuess(Intent intent) {
                                    byte send[] = intent.getByteArrayExtra("imgbuff");


                                    Signature.getInstance().setBuffer(send);
//                                    sendOK();
                                    SendReturnData(send, send.length);
                                }
                                @Override
                                public void onRetPain(Intent intent) {
                                    String sPaint = intent.getStringExtra("paint");
                                    byte send[] = sPaint.getBytes();
                                    SendReturnData(send, send.length);
                                }
                                @Override
                                public void onErr(int code) {
                                    byte send[] = new byte[]{(byte) 0x01, (byte) 0x00};
//                                    mApp.tts.speak("获取签名数据失败");
                                    SendData(send, send.length);

                                }
                            });
                            sendOK();
                        } catch (Exception ex) {

                            sendErr();
                        }
                        break;
                    }
                    case (byte) 0x02: {//取消签字
                        try {


                            Signature.getInstance().Exit();
                            sendOK();
                        } catch (Exception ex) {
                            Log.e(TAG, ex.getMessage());
                            sendErr();
                        }
                        break;
                    }
                    case (byte) 0x03: {//签字清屏
                        try {

                            Signature.getInstance().Clear();
                            sendOK();
                        } catch (Exception ex) {
                            Log.e(TAG, ex.getMessage());
                            sendErr();
                        }
                        break;
                    }
                    case (byte) 0x04: {//可以设置长宽
                        try {
                            int pos = 2;
                            int _len = data[pos] & 0xff;
                            byte[] h = new byte[_len];
                            System.arraycopy(data, ++pos, h, 0, _len);
                            pos += _len;
                            _len = data[pos] & 0xff;
                            byte[] w = new byte[_len];
                            System.arraycopy(data, ++pos, w, 0, _len);

                            String strheight = new String(h);
                            String strwidth = new String(w);

                            int heigth = Integer.parseInt(strheight);
                            int width = Integer.parseInt(strwidth);
                            App.getInstance().tts.speak(mApp.getString(R.string.PleaseSign));
                            Signature.getInstance().Start(App.getInstance().getApplicationContext(), heigth, width,
                                    new OnReturnListen() {
                                        @Override
                                        public void onSuess(Intent intent) {
                                            byte send[] = intent.getByteArrayExtra("imgbuff");

                                            Signature.getInstance().setBuffer(send);
//                                            sendOK();
                                            SendReturnData(send, send.length);
                                        }

                                        @Override
                                        public void onRetPain(Intent intent) {
                                            String sPaint = intent.getStringExtra("paint");
                                            byte send[] = sPaint.getBytes();
                                            SendReturnData(send, send.length);

                                        }

                                        @Override
                                        public void onErr(int code) {
                                            byte send[] = new byte[]{(byte) 0x01, (byte) 0x00};
                                            SendData(send, send.length);
                                        }
                                    });
                            //sendOK();
                        } catch (Exception ex) {
                            sendErr();
                        }
                        break;
                    }
                    case 05: {//获取签字数据
                        byte send[] = Signature.getInstance().getBuffer();
                        if (send != null) {
//                            Sys.Lib_Beep();
//                            mApp.tts.speak("获取成功");
                            SendReturnData(send, send.length);
                        } else {
//                            mApp.tts.speak("获取失败");
                         sendErr();
                        }
                        break;
                    }
                    case 06: {

                        Signature.getInstance().Save();
                    }

                }
                break;
            }
            //IC卡
            case (byte) 0x50: {//银行卡
                switch (data[1]) {
                    case (byte) 0x04: {//pboc
                        App.getInstance().tts.speak(mApp.getString(R.string.PleaseBankCard));
                        switch (data[2]) {

                            case (byte) 0x00: {
                                 try {
                                     //50040001020a413030303030303333330c4142434445464748494a4b4c0114
                                      ToolFun.Dalpey(200);
                                    if(String.valueOf(data[4])=="0"){//接触卡

                                        ICCard.getInstance().ShowActivity(App.getInstance().getApplicationContext(),Main2Activity.class);

                                    }else if (String.valueOf(data[4])=="1"){//非接
                                        ICCard.getInstance().ShowNFCActivity(App.getInstance().getApplicationContext(),Main2Activity.class);
                                    }
                                    byte[] send = ICCard.getInstance().GetICCInfo(data);
                                    if (send == null){
                                        Icc.Lib_IccClose((byte)0);
                                        Picc.Lib_PiccClose();
                                        ICCard.getInstance().CloseActivity(App.getInstance());
                                        App.getInstance().tts.speak("读卡失败");
                                        sendErr();

                                    }

                                    else{
                                        Icc.Lib_IccClose((byte)0);
                                        Picc.Lib_PiccClose();
                                        ICCard.getInstance().CloseActivity(App.getInstance());
                                        App.getInstance().tts.speak("读卡成功");
                                        SendReturnData(send, send.length);

                                    }
                                } catch (Exception ex) {
                                     Icc.Lib_IccClose((byte)0);
                                     Picc.Lib_PiccClose();
                                    ICCard.getInstance().CloseActivity(App.getInstance());
                                    sendErr();
                                }

                            }
                            Icc.Lib_IccClose((byte)0);
                            Picc.Lib_PiccClose();
                            ICCard.getInstance().CloseActivity(App.getInstance());
                            break;
                            case (byte) 0x01: {
                                try {
                                    if(String.valueOf(data[4])=="0"){//接触卡

                                        ICCard.getInstance().ShowActivity(App.getInstance().getApplicationContext(),Main2Activity.class);

                                    }else if (String.valueOf(data[4])=="1"){//非接
                                        ICCard.getInstance().ShowNFCActivity(App.getInstance().getApplicationContext(),Main2Activity.class);
                                    }
                                    byte[] send = ICCard.getInstance().GetARQC(data);
                                    ICCard.getInstance().CloseActivity(App.getInstance());
                                    if (send == null && send.length<100){
                                        Icc.Lib_IccClose((byte)0);
                                        Picc.Lib_PiccClose();
                                        ICCard.getInstance().CloseActivity(App.getInstance());
                                        App.getInstance().tts.speak("读卡失败");
                                        sendErr();
                                    }
                                    else{
                                        Icc.Lib_IccClose((byte)0);
                                        Picc.Lib_PiccClose();
                                        ICCard.getInstance().CloseActivity(App.getInstance());
                                        App.getInstance().tts.speak("读卡成功");
                                        SendReturnData(send, send.length);
                                    }


                                } catch (Exception ex) {
                                    ICCard.getInstance().CloseActivity(App.getInstance());
                                    sendErr();
                                }

                            }
                            ICCard.getInstance().CloseActivity(App.getInstance());
                            break;
                            case (byte) 0x02: {
                                try {
                                    byte[] send = ICCard.getInstance().ARPCExeScript(data);
                                    if (send == null){
                                        sendErr();
                                    }
                                    else{

                                        SendReturnData(send, send.length);
                                    }

                                } catch (Exception ex) {
                                    sendErr();
                                }

                            }
                            break;
                            case (byte) 0x03: {
                                try {
                                    byte[] send = ICCard.getInstance().GetTrDetail(data);
                                    if (send == null){
                                        sendErr();

                                    }
                                    else{

                                        SendReturnData(send, send.length);

                                    }
                                } catch (Exception ex) {
                                    sendErr();
                                }

                            }
                            break;
                            case (byte) 0x04: {
                                try {
                                    byte[] send = ICCard.getInstance().GetLoadLog(data);
                                    if (send == null)
                                        sendErr();
                                    else
                                        SendReturnData(send, send.length);
                                } catch (Exception ex) {
                                    sendErr();
                                }

                            }
                            break;
                            case (byte) 0x05: {
                                try {
                                    byte[] send = ICCard.getInstance().GetICAndARQCInfo(data);
                                    if (send == null)
                                        sendErr();
                                    else
                                        SendReturnData(send, send.length);
                                } catch (Exception ex) {
                                    sendErr();
                                }

                            }
                            break;
                            case(byte) 0x06:
                                  try {
                                    byte[] send = ICCard.getInstance().GetPSAM(data);
                                    if (send == null)
                                        sendErr();
                                    else
                                        SendReturnData(send, send.length);
                                } catch (Exception ex) {
                                    sendErr();
                                }
                                break;
                        }
                    }
                    break;
                    default:
                        sendErr();
                        break;
                }
            }
            break;
        }
    }

    private void SendData(byte[] writedata, int writeLen) {
        int len = writeLen;
        byte[] Writebuffer = new byte[len + 5];
        Writebuffer[0] = 2;
        Writebuffer[1] = (byte) (len >> 8);
        Writebuffer[2] = (byte) (len % 256);
        System.arraycopy(writedata, 0, Writebuffer, 3, writeLen);
        Writebuffer[3 + writeLen] = cr_bcc(writedata);
        Writebuffer[4 + writeLen] = 3;
        String sendStr = "";

        for (byte buff : Writebuffer) {
            sendStr = sendStr + String.format("%02X", buff) + " ";
        }

        try {
            App.getInstance().getBt().writeBt(Writebuffer, Writebuffer.length);
        } catch (Exception e) {

        }
    }

    private void SendReturnData(byte[] writedata, int writeLen) {
        int len = writeLen + 2;
        byte[] Writebuffer = new byte[len + 5];
        Writebuffer[0] = 2;
        Writebuffer[1] = (byte) (len >> 8);
        Writebuffer[2] = (byte) (len % 256);
        Writebuffer[3] = 0;
        Writebuffer[4] = 0;
        System.arraycopy(writedata, 0, Writebuffer, 5, writeLen);
        Writebuffer[5 + writeLen] = cr_bcc(writedata);
        Writebuffer[6 + writeLen] = 3;
        try {
            App.getInstance().getBt().writeBt(Writebuffer, Writebuffer.length);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }


    private byte cr_bcc(byte[] data) {
        byte temp = 0;
        for (byte item :
                data) {
            temp ^= item;
        }
        return temp;
    }

    private byte[] getData(byte[] src) {
        if (src[0] != 0x02)
            return null;
        int len = (src[1] & 0xff << 8) + src[2] & 0xff;
        if (src[len + 4] != (byte) 0x03)
            return null;
        byte[] dest = new byte[len];
        System.arraycopy(src, 3, dest, 0, len);
        byte crc = cr_bcc(dest);
        if (src[len + 3] != crc)
            return null;
        return dest;
    }


    static final byte[] err = {(byte) 0x02, (byte) 0x00, (byte) 0x02, (byte) 0x00, (byte) 0x01, (byte) 0x01, (byte) 0x03};
    static final byte[] ok = {(byte) 0x02, (byte) 0x00, (byte) 0x02, (byte) 0x00, (byte) 0x00, (byte) 0x00, (byte) 0x03};

    private void sendErr()

    {
        try {
            App.getInstance().getBt().writeBt(err, err.length);
        } catch (Exception e) {
        }
    }

    private void sendOK() {
        try {
            App.getInstance().getBt().writeBt(ok, ok.length);
        } catch (Exception e) {
        }
    }

    interface OnDataProcessingEventLisnter {
        void onCardDected();

        void onCancel();

        void onError(String error);

        void onFinish(String[] result);
    }


}
