package com.joesmate.a21.backgroundservices.bin;

import android.app.Application;
import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.widget.ImageView;

import com.joesmate.a21.backgroundservices.App;
import com.joesmate.a21.backgroundservices.KeyboradActivity;
import com.joesmate.a21.sdk.KeyboardDev;
import com.joesmate.sdk.util.ToolFun;
import com.jostmate.IListen.OnReturnListen;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;

import vpos.apipackage.Key;
import vpos.apipackage.PasswordShow;
import vpos.apipackage.Pci;

/**
 * Created by andre on 2017/7/25 .
 */

public class Keyboard {


    int m360fd = -1;

    public void setFD(int fd) {
        m360fd = fd;
    }

    public   Keyboard( ) {

    }
    /**
     * 显示界面
     */
    public void ShowActivity(Context context, Class<?> cls) {

        Intent intent = new Intent();
        intent.setClass(context,cls);
        intent.putExtra("action",7);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        context.startActivity(intent);
    }
    public void ShowActivity(Context context, Class<?> cls,byte[] data,OnReturnListen listen) {
        App.getInstance().setOnReturnListen(listen);
        Intent intent = new Intent();
        intent.setClass(context,cls);
        intent.putExtra("action",7);
        intent.putExtra("data",data);

        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        context.startActivity(intent);
    }
    public  void CloseActivity(Application app)
    {
        Intent intent = new Intent("action.view");
        intent.putExtra("action", 1);
        app.sendBroadcast(intent);
    }
    public static Keyboard getInstance() {

        return new Keyboard();
    }

    /**
     * 获取密钥
     *
     * @param data
     * @return
     */
    public byte[]    getPassword(byte[] data) throws UnsupportedEncodingException {
        return null;
//        int pos = 3;
//        int len = data[pos] & 0xff;
//        byte[] iencryType = new byte[len];
//        System.arraycopy(data, ++pos, iencryType, 0, len);
//        pos += len;
//
//        len = data[pos] & 0xff;
//        byte[] iTimes = new byte[len];
//        System.arraycopy(data, ++pos, iTimes, 0, len);
//        pos += len;
//
//        len = data[pos] & 0xff;
//        byte[] iLength = new byte[len];
//        System.arraycopy(data, ++pos, iLength, 0, len);
//        pos += len;
//
//        len = data[pos] & 0xff;
//        byte[] strVoice = new byte[len];
//        System.arraycopy(data, ++pos, strVoice, 0, len);
//        pos += len;
//
//        len = data[pos] & 0xff;
//        byte[] EndType = new byte[len];
//        System.arraycopy(data, ++pos, EndType, 0, len);
//        pos += len;
//
//        len = data[pos] & 0xff;
//        byte[] strTimeout = new byte[len];
//        System.arraycopy(data, ++pos, strTimeout, 0, len);
//        // pos += len;
//
//        int enctryTppe = iencryType[0] & 0xff;
//        int times = iTimes[0] & 0xff;
//        int length = iLength[0] & 0xff;
//        String voice = new String(strVoice);
//        int endtype = EndType[0] & 0xff;
//      final   int timeout = strTimeout[0] & 0xff;
//      Log.e("超时==",timeout+"");
//
//
//
//        String hexstr = ToolFun.strToHex(voice);
//        byte[] cardNo = ToolFun.hexStringToBytes(hexstr);
//        byte[] iAmount = ToolFun.hexStringToBytes("3132333435362e30300000000000");
//        byte mark = 0;
//        byte mode = 0;
//        int iii=  Key.Lib_KbFlush();
//        byte[] pinBlock = new byte[8];


//        new Thread(new Runnable() {
//
//            @Override
//            public void run() {
//                try {
//                    while(System.currentTimeMillis()-start<30*1000){
//                        keyNum = PasswordShow. Lib_GetPinEvent();
//                        Log.e("密码键盘==",""+keyNum);
//
//                        if (keyNum==6){
//
//
//                            App.getInstance().tts.speak("请提交");
//                        }
//                    }
//                } catch (Exception e) {
//                    e.printStackTrace();
//                }
//            }
//        }).start();


    }
    int  keyNum;

    /**
     * 初始化密码键盘
     *
     * @return
     */
    public int InitPinPad() {
        return KeyboardDev.getInstance().RestDefaultKey(m360fd);
    }

    /**
     * 主密钥
     *
     * @param data
     * @return
     */
    public int DownMKey(byte[] data) {
        int pos = 3;
        int len = data[pos] & 0xff;

        byte[] index = new byte[len];
        System.arraycopy(data, ++pos, index, 0, len);
        pos += len;

        len = data[pos] & 0xff;
        byte[] length = new byte[len];
        System.arraycopy(data, ++pos, length, 0, len);
        pos += len;

        len = data[pos] & 0xff;
        byte[] Zmk = new byte[len];
        System.arraycopy(data, ++pos, Zmk, 0, len);
        int ZmkIndex = index[0], ZmkLength = length[0];

        if (ZmkLength == 8)
            ZmkLength = 8;
        else if (ZmkLength == 16)
            ZmkLength = 16;
        else if (ZmkLength == 32)
            ZmkLength = 32;
        /***
         * byte  key_no[in]	密钥序号,取值范围0~99，其他值非法。
         byte   key_len [in]
         密钥长度,只能取8 、16、24这3个值，其他值非法。
         byte[]   key_data  [in]	密钥数据　
         byte   mode[in]	写入模式
         0x00：直接写入；
         0x01：使用旧的主密钥进行加密；(暂不支持)
         0x81：使用旧的主密钥进行解密；(暂不支持)
         0xff:与旧密钥进行异或后再写入；(暂不支持)
         * */
        int i=   Pci.Lib_PciWritePIN_MKey((byte)ZmkIndex,(byte)ZmkLength,Zmk,(byte)0);
        if(i==0){
            Log.e("主密钥下载成功",i+"");
        }else{
            Log.e("主密钥下载失败",i+"");
        }
        return i;
        //KeyboardDev.getInstance().DownMkey(m360fd, ZmkIndex, ZmkLength, Zmk);
    }

    /**
     * 明文更新主密钥
     *
     * @param data
     * @return
     */
    public int DownMKey2(byte[] data) {
        int pos = 3;
        int len = data[pos] & 0xff;

        byte[] index = new byte[len];
        System.arraycopy(data, ++pos, index, 0, len);
        pos += len;

        len = data[pos] & 0xff;
        byte[] length = new byte[len];
        System.arraycopy(data, ++pos, length, 0, len);
        pos += len;

        len = data[pos] & 0xff;
        byte[] Zmk = new byte[len];
        System.arraycopy(data, ++pos, Zmk, 0, len);
        int ZmkIndex = index[0], ZmkLength = length[0];

        if (ZmkLength == 8)
            ZmkLength = 1;
        else if (ZmkLength == 16)
            ZmkLength = 2;
        else if (ZmkLength == 32)
            ZmkLength = 3;
        return KeyboardDev.getInstance().DownMkey(m360fd, ZmkIndex, ZmkLength, Zmk);
    }

    /**
     * 下载工作密钥
     *
     * @param data
     * @return
     */
    public int DownWKey(byte[] data) {
        int pos = 3;
        int len = data[pos] & 0xff;

        byte[] Mindex = new byte[len];
        System.arraycopy(data, ++pos, Mindex, 0, len);
        pos += len;

        byte[] Windex = new byte[len];
        System.arraycopy(data, ++pos, Windex, 0, len);
        pos += len;

        len = data[pos] & 0xff;
        byte[] length = new byte[len];
        System.arraycopy(data, ++pos, length, 0, len);
        pos += len;

        len = data[pos] & 0xff;
        byte[] Zwk = new byte[len];
        System.arraycopy(data, ++pos, Zwk, 0, len);
        int ZmkIndex = Mindex[0], ZwkIndex = Windex[0], ZmkLength = length[0];
        if (ZmkLength == 8)
            ZmkLength = 8;
        else if (ZmkLength == 16)
            ZmkLength = 16;
        else if (ZmkLength == 32)
            ZmkLength = 32;
        /**
         *  byte key_no[in]	PIN密钥序号，取值范围0~9，其他值非法。
         byte   key_len[in]	密钥长度,只能取8、16、24这3个值，其他值非法。
         byte[]  key_data[in]	密钥数据
         byte   mode[in]	写入模式
         0X00：直接写入
         0X01：利用主密钥进行加密
         0X81：利用主密钥进行解密

         byte   mkey_no[in]	主密钥序号，取值范围0~9，其他值非法。

         * **/

        int i=  Pci.Lib_PciWritePinKey((byte)ZwkIndex,(byte)ZmkLength,Zwk,(byte)81,(byte)0);
        if(i==0){
            Log.e("工作密钥下载成功",i+"");
        }else{
            Log.e("工作密钥下载失败",i+"");
        }
        return i;
        //KeyboardDev.getInstance().DownWkey(m360fd, ZmkIndex, ZwkIndex, ZmkLength, Zwk);
    }


    /**
     * 激活工作密钥
     *
     * @param data
     * @return
     */
    public int ActiveWKey(byte[] data) {
        int pos = 3;
        int len = data[pos] & 0xff;

        byte[] Mindex = new byte[len];
        System.arraycopy(data, ++pos, Mindex, 0, len);
        pos += len;

        byte[] Windex = new byte[len];
        System.arraycopy(data, ++pos, Windex, 0, len);
        int MKeyIndex = Mindex[0], WKeyIndex = Windex[0];
        return KeyboardDev.getInstance().ActiveWKey(m360fd, MKeyIndex, WKeyIndex);
    }
}
