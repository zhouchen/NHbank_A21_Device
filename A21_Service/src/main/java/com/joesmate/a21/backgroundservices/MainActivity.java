package com.joesmate.a21.backgroundservices;


import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.media.AudioManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.os.PowerManager;
import android.support.annotation.RequiresApi;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.TextView;

import com.joesmate.a21.backgroundservices.ViewPagerAdapter;
import com.joesmate.a21.backgroundservices.bin.DeviceData;
import com.joesmate.a21.io.GPIO;
import com.joesmate.a21.sdk.BtStaDev;
import com.joesmate.a21.sdk.ReaderDev;
import com.joesmate.a21.serial_port_api.libserialport_api;

import java.util.ArrayList;
import java.util.List;

import vpos.apipackage.Sys;

public class MainActivity extends AppCompatActivity {
    private Intent intent;
    TextView txt_ver;
    TextView txt_info;

    AudioManager mAudioManager;
    GPIO gpio4 = new GPIO(4, 0);
    ViewPager viewPager;
    ViewPagerAdapter adapter;
    List<Integer> data;
    List<String> titleData;
    int currentPage = 0;
    int i;
    boolean viewPagerScrollStatus = false; //标志位,当在手动翻页时,自动翻页暂停

    TextView title, indicator;

    Handler handler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
          refresh();
            if (!viewPagerScrollStatus) {
                viewPager.setCurrentItem(++currentPage);
            }

            handler.sendEmptyMessageDelayed(1, 9000);

        }
    };



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_DISMISS_KEYGUARD,
                WindowManager.LayoutParams.FLAG_DISMISS_KEYGUARD);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON,
                WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

        requestWindowFeature(Window.FEATURE_NO_TITLE);

        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);

        setContentView(R.layout.activity_main);

        intent = new Intent(MainActivity.this, BTSerialPortService.class);
        viewPager = (ViewPager) findViewById(R.id.viewpager);
        mAudioManager = (AudioManager) getSystemService(Context.AUDIO_SERVICE);
        startService(intent);
        screenOn();
        Sys.Lib_PowerOff();
        Sys.Lib_PowerOn();
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    Thread.sleep(3000);
                    App.getInstance().tts.speak("欢迎使用");
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }).start();

        title = (TextView) findViewById(R.id.banner_title);
        indicator = (TextView) findViewById(R.id.banner_indicator);

        data = new ArrayList<>();

        titleData = new ArrayList<>();


        data.add(R.mipmap.a3);     //titleData.add("3.没事");
        data.add(R.mipmap.a1);    // titleData.add("1.哦");
        data.add(R.mipmap.a2);    // titleData.add("2.怎么了");
        data.add(R.mipmap.a3);    // titleData.add("3.没事");
        data.add(R.mipmap.a1);    // titleData.add("1.怎么了");
//        title.setText("1/3");
        adapter = new ViewPagerAdapter(data);
        viewPager.setAdapter(adapter);

        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                //头尾衔接,无限循环
                if (position == data.size() - 1) {
                    currentPage = 1;
                } else if (position == 0) {
                    currentPage = data.size() - 2;
                } else {
                    currentPage = position;
                }
            }

            @Override
            public void onPageScrollStateChanged(int state) {
                if (state == ViewPager.SCROLL_STATE_IDLE) {
                    viewPagerScrollStatus = false;
                    viewPager.setCurrentItem(currentPage, false);

                    indicator.setText(currentPage + "/" + (5 - 2));
                } else {
                    viewPagerScrollStatus = true;
                }
            }
        });
        handler.sendEmptyMessageDelayed(1, 800);

        viewPager.setPageTransformer(true, new ViewPager.PageTransformer() {
            @Override
            public void transformPage(View page, float position) {

                int width = page.getWidth();
                //我们给不同状态的页面设置不同的效果
                //通过position的值来分辨页面所处于的状态
                if (position < -1) {//滑出的页面
                    page.setScrollX((int) (width * 0.75 * -1));
                } else if (position <= 1) {//[-1,1]
                    if (position < 0) {//[-1,0]
                        page.setScrollX((int) (width * 0.75 * position));
                    } else {//[0,1]
                        page.setScrollX((int) (width * 0.75 * position));
                    }
                } else {//即将滑入的页面
                    page.setScrollX((int) (width * 0.75));
                }
            }
        });

    }

    @Override
    protected void onResume() {
        /**
         * 设置为横屏
         */
        if (getRequestedOrientation() != ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE) {
//      setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_REVERSE_LANDSCAPE);
        }
        super.onResume();

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        stopService(intent);
        Sys.Lib_PowerOff();
        BtStaDev.getInstance().BtPowerOff();
    }

    public void screenOn() {//亮屏
        // turn on screen
        PowerManager mPowerManager = (PowerManager) getSystemService(POWER_SERVICE);
        PowerManager.WakeLock mWakeLock = mPowerManager.newWakeLock(PowerManager.SCREEN_BRIGHT_WAKE_LOCK | PowerManager.ACQUIRE_CAUSES_WAKEUP, "tag");
        mWakeLock.acquire();
        mWakeLock.release();
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    private void refresh() {
        DeviceData deviceData = DeviceData.getInstance();


        title.setText("电量：" + deviceData.getPower() + "%");

    }
}
