package com.joesmate.a21.backgroundservices;

import android.annotation.SuppressLint;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.ActivityInfo;
import android.media.Image;
import android.os.AsyncTask;
import android.os.Handler;
import android.os.Message;
import android.os.PowerManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Window;
import android.view.WindowManager;
import android.webkit.WebView;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.joesmate.a21.backgroundservices.bin.Keyboard;
import com.joesmate.a21.sdk.BtStaDev;
import com.joesmate.sdk.util.LogMg;
import com.joesmate.sdk.util.ToolFun;
import com.jostmate.IListen.OnReturnListen;
import java.io.UnsupportedEncodingException;
import vpos.apipackage.Key;
import vpos.apipackage.PasswordShow;
import vpos.apipackage.Pci;
import vpos.apipackage.Sys;



public class KeyboradActivity extends AppCompatActivity {

    private WebView webView;
    private Intent _intent;
    private boolean isboolean = false;
    private TextView image;
    boolean isExite = true;
    boolean isTimeOut = false;
    byte[] keydata = null;
    OnReturnListen listen;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_DISMISS_KEYGUARD,
                WindowManager.LayoutParams.FLAG_DISMISS_KEYGUARD);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON,
                WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
//        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);//强制横屏
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        _intent = getIntent();
        setContentView(R.layout.activity_keybord);
        keydata = _intent.getByteArrayExtra("data");
        webView = findViewById(R.id.wMyWebView);
        image = (TextView) findViewById(R.id.textView2);
        listen = App.getInstance().getOnReturnListen();
        isTimeOut = false;
        isExite = false;
        timeOutTask.execute(30);//30秒
        Loadhtml();
        screenOn();
        IntentFilter filter = new IntentFilter();
        filter.addAction("action.view");
        isboolean = true;
        registerReceiver(broadcastReceiver, filter);


    }

    public void screenOn() {
        // turn on screen
        PowerManager mPowerManager = (PowerManager) getSystemService(POWER_SERVICE);
        PowerManager.WakeLock mWakeLock = mPowerManager.newWakeLock(PowerManager.SCREEN_BRIGHT_WAKE_LOCK | PowerManager.ACQUIRE_CAUSES_WAKEUP, "tag");
        mWakeLock.acquire();
        mWakeLock.release();
    }

    private void Loadhtml() {
        int tag = _intent.getIntExtra("action", 0);
        switch (tag) {

            case 7:
                ShowKey();//密码键盘
                break;

        }
    }

    @Override
    protected void onDestroy() {
        isExite = true;
        super.onDestroy();
        finish();
    }

    private void ShowKey() {
        webView.loadUrl("file:///android_asset/key.html");
    }


    final BroadcastReceiver broadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            int tag = intent.getIntExtra("action", -1);
            switch (tag) {

                case 1://取消
                {
                    Message msg = handler.obtainMessage();
                    msg.what = 1;
                    msg.obj = intent;
                    handler.sendMessage(msg);
                }
                break;
                default: {
                }

            }
        }
    };
    @SuppressLint("HandlerLeak")
    final Handler handler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            switch (msg.what) {

                case 1: {//关闭界面

                   KeyboradActivity.this.finish();
                    break;
                }
            }
        }
    };

    @Override
    protected void onResume() {
        /**
         * 设置为横屏
         */
        if (getRequestedOrientation() != ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE)
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_REVERSE_LANDSCAPE);

        super.onResume();

    }

    private void passwor() throws UnsupportedEncodingException {

        int pos = 3;
        int len = keydata[pos] & 0xff;
        byte[] iencryType = new byte[len];
        System.arraycopy(keydata, ++pos, iencryType, 0, len);
        pos += len;

        len = keydata[pos] & 0xff;
        byte[] iTimes = new byte[len];
        System.arraycopy(keydata, ++pos, iTimes, 0, len);
        pos += len;

        len = keydata[pos] & 0xff;
        byte[] iLength = new byte[len];
        System.arraycopy(keydata, ++pos, iLength, 0, len);
        pos += len;

        len = keydata[pos] & 0xff;
        byte[] strVoice = new byte[len];
        System.arraycopy(keydata, ++pos, strVoice, 0, len);
        pos += len;

        len = keydata[pos] & 0xff;
        byte[] EndType = new byte[len];
        System.arraycopy(keydata, ++pos, EndType, 0, len);
        pos += len;

        len = keydata[pos] & 0xff;
        byte[] strTimeout = new byte[len];
        System.arraycopy(keydata, ++pos, strTimeout, 0, len);
        // pos += len;

        int enctryTppe = iencryType[0] & 0xff;
        int times = iTimes[0] & 0xff;
        int length = iLength[0] & 0xff;
        String voice = new String(strVoice);
        int endtype = EndType[0] & 0xff;
        final int timeout = strTimeout[0] & 0xff;
        byte[] pinBlock = new byte[8];


        String hexstr = ToolFun.strToHex(voice);
        byte[] cardNo = ToolFun.hexStringToBytes(hexstr);
        byte[] iAmount = ToolFun.hexStringToBytes("3132333435362e30300000000000");
        byte mark = 0;
        byte mode = 0;
        int ret = Pci.Lib_PciGetPin((byte) enctryTppe, (byte) length, (byte) length, mode, cardNo, pinBlock, mark, iAmount, (byte) timeout, null);//42 9A B0 C2 06 60 D7 95

        if (ret == 0) {
            Intent intent = new Intent();
            intent.putExtra("pinBlock", pinBlock);
            App.getInstance().tts.speak("密码获取成功");
            listen.onSuess(intent);

        } else {
            App.getInstance().tts.speak("密码获取失败");
            listen.onErr(ret);

        }
    }

    protected void onPause() {
        if (isboolean) {
            isboolean = false;
            unregisterReceiver(broadcastReceiver);
        }
        super.onPause();
    }

    int keyNum;
    private final Thread getPWTh = new Thread(new Runnable() {
        @Override
        public void run() {
            try {
                passwor();
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }
        }
    });
    @SuppressLint("StaticFieldLeak")
    private final AsyncTask<Integer, String, String> timeOutTask = new AsyncTask<Integer, String, String>() {
        @Override
        protected String doInBackground(Integer... ints) {
            final long timeout = ints[0] * 1000;
            long startTime = System.currentTimeMillis();
            while (true) {
                if (System.currentTimeMillis() - startTime > timeout) {
                    isTimeOut = true;

                    break;
                }
                if (isExite) {
                    break;
                }
                keyNum = PasswordShow.Lib_GetPinEvent();
                if (keyNum > 0) {
                    publishProgress(String.valueOf(keyNum));
                }
            }
            return "";
        }

        @Override
        protected void onProgressUpdate(String... values) {
            super.onProgressUpdate(values);

            switch (values[0]){

                case "1":
                    image.setText(" *");
                    break;
                case "2":

                    image.setText(" *  *");
                    break;
                case "3":
                    image.setText(" *  *  *");
                    break;
                case "4":
                    image.setText(" *" + "  *" + "  *" + "  *");
                    break;
                case "5":
                    image.setText(" *" + " \r*" + " \r*" + " \r*" + " \r*");
                    break;
                case "6":

                    image.setText(" *" + " \r*" + " \r*" + " \r*" + " \r*" + " \r*");
                    App.getInstance().tts.speak("密码长度输入超限，请提交");
                    break;

            }
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            getPWTh.start();
        }

        @Override
        protected void onPostExecute(String s) {

            super.onPostExecute(s);
            if (isTimeOut) {
                listen.onErr(-3);
                finish();
            }
        }
    };


}
