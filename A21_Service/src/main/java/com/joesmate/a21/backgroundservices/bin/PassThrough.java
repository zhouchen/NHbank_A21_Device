package com.joesmate.a21.backgroundservices.bin;

import android.app.Application;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

import com.joesmate.a21.backgroundservices.App;
import com.joesmate.a21.io.GPIO;
import com.joesmate.a21.serial_port_api.libserialport_api;
import com.joesmate.sdk.util.ToolFun;

import java.util.Arrays;

import vpos.apipackage.Fingerprint;
import vpos.apipackage.Mcr;
import vpos.apipackage.Sys;

/**
 * Created by andre on 2017/7/20 .
 */

public class PassThrough {
    private PassThrough() {
    }

    //private static int m_fpfd = App.getInstance().m_fpfd;

    private static byte[] fingerbuffer ;
    private static int[] fingerlen = new int[4092];
    private static final PassThrough mInstance = new PassThrough();
    /**
     * 显示界面
     */
    public void ShowActivity(Context context, Class<?> cls) {

        Intent intent = new Intent();
        intent.setClass(context,cls);
        intent.putExtra("action",3);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        context.startActivity(intent);
    }

    public  void CloseActivity(Application app)
    {
        Intent intent = new Intent("action.view");
        intent.putExtra("action", 1);
        app.sendBroadcast(intent);
    }
    App mApp = App.getInstance();
    public static PassThrough getInstance() {
        return mInstance;
    }
    int iRet;
    public int SendCMD(byte[] cmd) {

        fingerbuffer = new byte[4092];
        long startTime = System.currentTimeMillis();
        while (System.currentTimeMillis()-startTime<30000) {
       if (fingerbuffer[0]==0) {
           iRet = Fingerprint.Lib_FpCommunication(cmd, cmd.length, fingerbuffer, fingerlen, 4000);
           if ( fingerbuffer[0] == 126 &&fingerlen[0] >0)
           {
               iRet = 0;
           } else {
               iRet = -1;
                 }
          }else{
           return 0;
                }
    }
    return iRet;
    }
    public byte[] getBuffer(int[] reCode) {
        reCode[0] = fingerlen[0];

        if (fingerlen[0] <= 0  && fingerbuffer[0]==00) {
            Fingerprint.Lib_FpClose();
            return new byte[]{(byte) 0x30, (byte) 0x11};
        }else {
            byte[] buff = new byte[fingerlen[0]];
            System.arraycopy(fingerbuffer, 0, buff, 0, fingerlen[0]);

            Fingerprint.Lib_FpClose();
            return buff;
        }
    }
    public int setBaud(int baud) {
        int b = 9600;
        switch (baud) {
            case 0:
                b = 9600;
                break;
            case 1:
                b = 19200;
                break;
            case 2:
                b = 38400;
                break;
            case 3:
                b = 57600;
                break;
            case 4:
                b = 115200;
                break;
        }
        return Fingerprint.Lib_SetFgBaudrate(b);
    }
}
