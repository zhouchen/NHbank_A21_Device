package com.joesmate.a21.backgroundservices;

import android.app.Application;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.os.IBinder;
import android.util.Log;

import com.jl.pinpad.IRemotePinpad;
import com.joesmate.AndroidTTS.*;
import com.joesmate.BaesTextToSpeech;
import com.joesmate.a21.sdk.ReaderDev;
import com.joesmate.a21.serial_port_api.libserialport_api;
import com.joesmate.gpio.BaseGpio;
import com.joesmate.gpio.GpioFactory;
import com.jostmate.IListen.OnReturnListen;
import com.jostmate.btfactory.BtFactory;
import com.jostmate.ibt.BaseBT;
//import com.jollytech.app.Platform;
import java.io.File;
import java.util.List;
import java.util.concurrent.Executors;

import vpos.apipackage.Sys;

/**
 * Created by andre on 2017/7/3 .
 */

public class App extends Application {
    public BaseGpio btGpio = GpioFactory.INSTANCE.createBtGpio();
    public BaseGpio financiaModGpio = GpioFactory.INSTANCE.createFinanciaModGpio();
    public BaseGpio financiaModWorkStateGpio = GpioFactory.INSTANCE.createFinanciaModWorkStateGpio();
    public BaseGpio RS232Gpio = GpioFactory.INSTANCE.createRs232Gpio();
    public BaseGpio EHandwriteGpio = GpioFactory.INSTANCE.createEHandwriteGpio();
    private static final String TAG = "com.joesmate.a21.backgroundservices.App";
    private static App mApp;
    public BaseBT btFactory;
    public IRemotePinpad m_pinpad;

    public BaesTextToSpeech tts;

    public int m_btfd = -1;



    @Override
    public void onCreate() {
        super.onCreate();
        mApp = this;
        tts = new TextToSpeechEx(this.getApplicationContext());
        btGpio.offPower();//关闭蓝牙模块
        EHandwriteGpio.offPower();//关闭电磁屏

        openBt();//打开蓝牙

        connectPinpad();
    }

    void connectPinpad() {

        Intent service = new Intent("com.remote.service.PINPAD");
        Intent eintent = new Intent(getExplicitIntent(mApp.getApplicationContext(), service));
        bindService(eintent, connection, Context.BIND_AUTO_CREATE);
    }

    ServiceConnection connection = new ServiceConnection() {

        @Override
        public void onServiceDisconnected(ComponentName arg0) {
            m_pinpad = null;
        }

        @Override
        public void onServiceConnected(ComponentName arg0, IBinder arg1) {
            m_pinpad = IRemotePinpad.Stub.asInterface(arg1);
            if (m_pinpad != null) {
            }
        }
    };

    public static App getInstance() {

        return mApp;
    }


    private OnReturnListen mlisten = null;

    public void setOnReturnListen(OnReturnListen listen) {
        mlisten = listen;
    }

    public OnReturnListen getOnReturnListen() {
        if (mlisten != null)
            return mlisten;
        return null;
    }

    public static Intent getExplicitIntent(Context context,
                                           Intent implicitIntent) {
        PackageManager pm = context.getPackageManager();
        List<ResolveInfo> resolveInfo = pm.queryIntentServices(implicitIntent,
                0);
        if (resolveInfo == null || resolveInfo.size() != 1) {
            return null;
        }
        ResolveInfo serviceInfo = resolveInfo.get(0);
        String packageName = serviceInfo.serviceInfo.packageName;
        String className = serviceInfo.serviceInfo.name;
        ComponentName component = new ComponentName(packageName, className);
        Intent explicitIntent = new Intent(implicitIntent);
        explicitIntent.setComponent(component);
        return explicitIntent;
    }

    public void openBt() {
        try {
            btFactory = BtFactory.CreateBT(this.getApplicationContext());
            btFactory.openBt();

        } catch (Exception e) {
            e.getMessage();
        }
    }

    public BaseBT getBt() {

        return btFactory;
    }

}
