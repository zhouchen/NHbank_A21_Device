package com.joesmate.a21.backgroundservices;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.ActivityInfo;
import android.os.Handler;
import android.os.Message;
import android.os.PowerManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Window;
import android.view.WindowManager;
import android.webkit.WebView;
import com.joesmate.sdk.util.LogMg;

public class Main2Activity extends AppCompatActivity {

    private WebView webView;
    private Intent _intent;
    private boolean isboolean=false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_DISMISS_KEYGUARD,
                WindowManager.LayoutParams.FLAG_DISMISS_KEYGUARD);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON,
                WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
//        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);//强制横屏
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        _intent = getIntent();
        setContentView(R.layout.activity_main2);
        webView = findViewById(R.id.wMyWebView);
        Loadhtml();
        screenOn();
        IntentFilter filter = new IntentFilter();
        filter.addAction("action.view");
        isboolean=true;
        registerReceiver(broadcastReceiver, filter);

    }
    public void screenOn() {
        // turn on screen
        PowerManager mPowerManager = (PowerManager) getSystemService(POWER_SERVICE);
        PowerManager.WakeLock mWakeLock = mPowerManager.newWakeLock(PowerManager.SCREEN_BRIGHT_WAKE_LOCK | PowerManager.ACQUIRE_CAUSES_WAKEUP, "tag");
        mWakeLock.acquire();
        mWakeLock.release();
    }
    private void Loadhtml() {
        int tag = _intent.getIntExtra("action", 0);
        switch (tag) {
            case 3: {
              ShowFinger();
            }
            break;
            case 8:
                ShowIDCard();//显示身份证

            break;
            case 4:
                ShowMag();//磁条卡
                break;
            case 5:
                ShowICCard();//接触式ic卡
                break;
            case 6:
                ShowNFC();//非接触式ic卡
                break;
//            case 7:
//                ShowKey();//密码键盘
//                break;
            default: {
            }
            break;
        }
    }

    private void ShowNFC(){webView.loadUrl("file:///android_asset/nfc.html");}
//    private void ShowKey(){webView.loadUrl("file:///android_asset/key.html");}
    private void ShowICCard(){webView.loadUrl("file:///android_asset/ic.html");}
    private void ShowMag(){webView.loadUrl("file:///android_asset/mag.html");}
    private void ShowFinger() {
        webView.loadUrl("file:///android_asset/Finger.html");
    }

    private void ShowIDCard() {
        webView.loadUrl("file:///android_asset/IDCard.html");
    }

    final BroadcastReceiver broadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            int tag = intent.getIntExtra("action", -1);
            switch (tag) {

                case 1://取消
                {
                    Message msg = handler.obtainMessage();
                    msg.what = 1;
                    msg.obj = intent;
                    handler.sendMessage(msg);
                }
                break;

                default: {
                    LogMg.e("页面显示", "广播数据不存在");
                }

            }
        }
    };
    final Handler handler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            switch (msg.what) {

                case 1: {//关闭界面

                    finish();
                    break;
                }
            }
        }
    };
    @Override
    protected void onResume() {
        /**
         * 设置为横屏
         */
        if(getRequestedOrientation()!= ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE){
//      setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_REVERSE_LANDSCAPE);
        }
        super.onResume();

    }
    protected void onPause(){
        if(isboolean){
            isboolean=false;
            unregisterReceiver(broadcastReceiver);
        }
        super.onPause();
    }
}
