package com.joesmate.a21.backgroundservices.bin;

import android.app.Application;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

import com.joesmate.a21.backgroundservices.App;
import com.joesmate.sdk.util.ToolFun;

import vpos.apipackage.Sys;

public class ICCard {
    /**
     * 显示界面 接触式ic卡
     */
    public void ShowActivity(Context context, Class<?> cls) {

        Intent intent = new Intent();
        intent.setClass(context,cls);
        intent.putExtra("action",5);//接触式
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        context.startActivity(intent);
    }
    /**
     * 显示界面 非接触式ic卡
     */
    public void ShowNFCActivity(Context context, Class<?> cls) {

        Intent intent = new Intent();
        intent.setClass(context,cls);
        intent.putExtra("action",6);//接触式
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        context.startActivity(intent);
    }
    public  void CloseActivity(Application app)
    {
        Intent intent = new Intent("action.view");
        intent.putExtra("action", 1);
        app.sendBroadcast(intent);
    }

        App mApp = App.getInstance();

        private ICCard() {
        }

        private static final ICCard mInstance = new ICCard();

        public static ICCard getInstance() {

            return mInstance;
        }

    public byte[] GetICCInfo(byte[] data) {
        Sys.Lib_Beep();
        int pos = 3;
        int len = data[pos] & 0xff;
        byte[] ictype = new byte[len];
        System.arraycopy(data, ++pos, ictype, 0, len);

        pos += len;
        len = data[pos] & 0xff;
        byte[] aidList = new byte[len];
        System.arraycopy(data, ++pos, aidList, 0, len);

        pos += len;
        len = data[pos] & 0xff;
        byte[] tagList = new byte[len];
        System.arraycopy(data, ++pos, tagList, 0, len);


        pos += len;
        len = data[pos] & 0xff;
        byte[] timeout = new byte[len];
        System.arraycopy(data, ++pos, timeout, 0, len);

        String[] str = PBOC.getInstance().GetICCInfo(ictype[0], new String(aidList), new String(tagList), timeout[0] & 0xff);
        byte[] arrRet = null;
        if (str != null)
            arrRet = String.format("%s|%s", str[0], str[1]).getBytes();
        return arrRet;
    }

    public byte[] GetARQC(byte[] data) {
        Sys.Lib_Beep();
        int pos = 3;
        int len = data[pos] & 0xff;
        byte[] ictype = new byte[len];
        System.arraycopy(data, ++pos, ictype, 0, len);

        pos += len;
        len = data[pos] & 0xff;
        byte[] txtdata = new byte[len];
        System.arraycopy(data, ++pos, txtdata, 0, len);

        pos += len;
        len = data[pos] & 0xff;
        byte[] aidList = new byte[len];
        System.arraycopy(data, ++pos, aidList, 0, len);

        pos += len;
        len = data[pos] & 0xff;
        byte[] timeout = new byte[len];
        System.arraycopy(data, ++pos, timeout, 0, len);

        String[] str = PBOC.getInstance().GetICCArqc(ictype[0], new String(txtdata), new String(aidList), timeout[0] & 0xff);
        byte[] arrRet = null;
        if (str != null) {
            arrRet = String.format("%s|%s", str[0], str[1]).getBytes();
        }
        return arrRet;
    }

    public byte[] ARPCExeScript(byte[] data) {
        int pos = 3;
        int len = data[pos] & 0xff;
        byte[] ictype = new byte[len];
        System.arraycopy(data, ++pos, ictype, 0, len);

        pos += len;
        len = data[pos] & 0xff;
        byte[] txtdata = new byte[len];
        System.arraycopy(data, ++pos, txtdata, 0, len);

        pos += len;
        len = data[pos] & 0xff;
        byte[] arpc = new byte[len];
        System.arraycopy(data, ++pos, arpc, 0, len);

        pos += len;
        len = data[pos] & 0xff;
        byte[] CDol2 = new byte[len];
        System.arraycopy(data, ++pos, CDol2, 0, len);

        String[] str = PBOC.getInstance().ARPCExeScript(ictype[0], new String(txtdata), new String(arpc), new String(CDol2));
        byte[] arrRet = null;
        if (str != null)
            arrRet = String.format("%s|%s|%s", str[0], str[1], str[3]).getBytes();
        return arrRet;
    }

    public byte[] GetTrDetail(byte[] data) {
        int pos = 3;
        int len = data[pos] & 0xff;
        byte[] ictype = new byte[len];
        System.arraycopy(data, ++pos, ictype, 0, len);

        pos += len;
        len = data[pos] & 0xff;
        byte[] NOLog = new byte[len];
        System.arraycopy(data, ++pos, NOLog, 0, len);


        pos += len;
        len = data[pos] & 0xff;
        byte[] timeout = new byte[len];
        System.arraycopy(data, ++pos, timeout, 0, len);

        String[] str = PBOC.getInstance().GetTrDetail(ictype[0], NOLog[0] & 0xff, timeout[0] & 0xff);
        byte[] arrRet = null;
        if (str != null)
            arrRet = String.format("%s|%s", str[0], str[1]).getBytes();
        return arrRet;
    }

    public byte[] GetLoadLog(byte[] data) {
        int pos = 3;
        int len = data[pos] & 0xff;
        byte[] ictype = new byte[len];
        System.arraycopy(data, ++pos, ictype, 0, len);

        pos += len;
        len = data[pos] & 0xff;
        byte[] NOLog = new byte[len];
        System.arraycopy(data, ++pos, NOLog, 0, len);

        pos += len;
        len = data[pos] & 0xff;
        byte[] aidList = new byte[len];
        System.arraycopy(data, ++pos, aidList, 0, len);

        pos += len;
        len = data[pos] & 0xff;
        byte[] timeout = new byte[len];
        System.arraycopy(data, ++pos, timeout, 0, len);

        String[] str = PBOC.getInstance().GetLoadLog(ictype[0], NOLog[0] & 0xff, new String(aidList), timeout[0] & 0xff);
        byte[] arrRet = null;
        if (str != null)
            arrRet = String.format("%s|%s", str[0], str[1]).getBytes();
        return arrRet;
    }

    public byte[] GetICAndARQCInfo(byte[] data) {
        int pos = 3;
        int len = data[pos] & 0xff;
        byte[] ictype = new byte[len];
        System.arraycopy(data, ++pos, ictype, 0, len);

        pos += len;
        len = data[pos] & 0xff;
        byte[] aidList = new byte[len];
        System.arraycopy(data, ++pos, aidList, 0, len);

        pos += len;
        len = data[pos] & 0xff;
        byte[] tagList = new byte[len];
        System.arraycopy(data, ++pos, tagList, 0, len);

        pos += len;
        len = data[pos] & 0xff;
        byte[] txtdata = new byte[len];
        System.arraycopy(data, ++pos, txtdata, 0, len);

        pos += len;
        len = data[pos] & 0xff;
        byte[] timeout = new byte[len];
        System.arraycopy(data, ++pos, timeout, 0, len);

        String[] str = PBOC.getInstance().GetICAndARQCInfo(ictype[0], new String(aidList), new String(tagList), new String(txtdata), timeout[0] & 0xff);
        byte[] arrRet = null;
        if (str != null)
            arrRet = String.format("%s|%s|%s", str[0], str[1], str[3]).getBytes();
        return arrRet;
    }


        public byte[] GetPSAM(byte[] data) {
        Sys.Lib_Beep();
        int pos = 3;
        int len = data[pos] & 0xff;
        byte[] ictype = new byte[len];
        System.arraycopy(data, ++pos, ictype, 0, len);

        pos += len;
        len = data[pos] & 0xff;
        byte[] aidList = new byte[len];
        System.arraycopy(data, ++pos, aidList, 0, len);

        pos += len;
        len = data[pos] & 0xff;
        byte[] tagList = new byte[len];
        System.arraycopy(data, ++pos, tagList, 0, len);


        pos += len;
        len = data[pos] & 0xff;
        byte[] timeout = new byte[len];
        System.arraycopy(data, ++pos, timeout, 0, len);

        String[] str = PBOC.getInstance().GetPSAM(ictype[0], new String(aidList), new String(tagList), timeout[0] & 0xff);
        byte[] arrRet = null;
        if (str != null){
            str[0]="0";
            str[1]="成功";
            arrRet = String.format("%s|%s", str[0], str[1]).getBytes();
            }
        return arrRet;
    }
}
